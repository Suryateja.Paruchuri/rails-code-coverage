class ArticlesController < ApplicationController
  def index
    data = calculate_data
    render json: { hello: "welcome", data: data }
  end

  private

  def calculate_data
    4
  end
end
